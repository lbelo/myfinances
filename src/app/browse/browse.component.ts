import { Component, OnInit } from "@angular/core";
import { DataService, IDataItem } from "../core/data.service";
var Sqlite = require("nativescript-sqlite");
import { DatabaseService } from "../core/sqlite.service";
import { NumberFormat } from "nativescript-intl";

@Component({
    selector: "Browse",
    moduleId: module.id,
    templateUrl: "./browse.component.html",
    styleUrls: ["./browse.component.scss"]
})
export class BrowseComponent implements OnInit {
    items: Array<IDataItem>;

    constructor(
        private dataService: DataService,
        private database: DatabaseService
    ) {
        // initialize
        console.log('init browse');
    }

    ngOnInit(): void {
        console.log('Browse Comp init');
    }

    /**
     * Get list of buttons and update display
     */
    refreshExpenseList(): void {
        this.database.getDBConnection().then(
            db => {
                db.resultType(Sqlite.RESULTSASOBJECT);
                db.all('SELECT * FROM expenses ORDER BY date DESC')
                .then(
                    (rows) => {
                        this.items = rows;
                    }
                );
            }
        );
    }

    // convert date into a readable string
    toDate(rawDate: string) {
        return (new Date(rawDate)).toDateString();
    }

    // convert number into currency format
    toCurrency(rawNumber: number)  {
        return new NumberFormat('en-US', {'style': 'currency', 'currency': 'USD', 'currencyDisplay': 'symbol'}, '#,##0.00').format(new Number(rawNumber));
    }

    
}
