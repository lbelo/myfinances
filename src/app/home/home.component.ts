/**
 * Component for Home Screen display where entry buttons are shown
 */
import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { DataService, IDataItem } from "../core/data.service";
import { ButtonService, IButtonItem } from "../core/button.service";
import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { EntryComponent } from "../entry/entry.component";
import { NumberFormat } from "nativescript-intl";
import * as Toast from "nativescript-toast";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss", "../browse/browse.component.scss"]
})

export class HomeComponent implements OnInit {
    buttons: Array<IButtonItem>;
    buttonSizeCssClass: string;
    latestEntries: Array<IDataItem>;

    constructor(
        private buttonService: ButtonService,
        private dataService: DataService, 
        private _modal: ModalDialogService,
        private _vcRef: ViewContainerRef
    ) { 
    }

    ngOnInit(): void {
        console.log('Home Comp init');
        this.getLatestEntries();

        // setup buttons
        this.refreshButtonList();
    }

    /**
     * Get lastest 3 items
     */
    getLatestEntries(): void {
        this.dataService.getLatestItems().then(
            (rows) => {
                this.latestEntries = rows;
            }
        );
    }

    /**
     * Get list of buttons and update display
     */
    refreshButtonList(): void {
        this.buttonService.getButtons().then(
            (rows) => { 
                this.buttons = rows;
                this.buttonSizeCssClass = "entry_button";
            }
        );
        this.getLatestEntries();
    }

    /**
     * Show modal for data entry
     * @param string button 
     */
    showAlert(button: IButtonItem): void {
        // show popup only if enabled for the button
        if (button.trigger_alert) {
            this._modal.showModal(EntryComponent, { context: { button }, fullscreen: false, viewContainerRef: this._vcRef }).then(() => {
                console.log('modal closed');
                this.getLatestEntries();
            });
        } else {
            console.log('direct save');
            this.dataService.addItem(button.category, button.amount, button.description);
            Toast.makeText("Expense has been logged.").show();
        }

        this.getLatestEntries();
    }

    toIcon(value: string) {
        return String.fromCharCode(parseInt(value, 16));
    }

    // convert date into a readable string
    toDate(rawDate: string) {
        return (new Date(rawDate)).toDateString();
    }

    // convert number into currency format
    toCurrency(rawNumber: number)  {
        return new NumberFormat('en-US', {'style': 'currency', 'currency': 'USD', 'currencyDisplay': 'symbol'}, '#,##0.00').format(new Number(rawNumber));
    }
}
