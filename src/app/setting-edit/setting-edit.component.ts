import { Component, OnInit } from '@angular/core';
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { ButtonService, IButtonItem } from "../core/button.service";
import * as Toast from "nativescript-toast";

@Component({
    selector: 'ns-setting-edit',
    templateUrl: './setting-edit.component.html',
    styleUrls: ['./setting-edit.component.scss'],
    moduleId: module.id,
})

export class SettingEditComponent implements OnInit {
    entry: IButtonItem;
    index: number;

    constructor(private params: ModalDialogParams, private buttonService: ButtonService) {
        // value must be initialized after being declared otherwise you can't use it
        this.entry = {
            category: params.context.button.category,
            icon: params.context.button.icon,
            description: params.context.button.description,
            amount: params.context.button.amount,
            // convert to boolean so we can toggle switch
            trigger_alert: params.context.button.trigger_alert? true : false
        };

        // id of firebase entry for buttons
        this.index = params.context.index;
    }

    closeAndSave(): void {
        console.log("attempting close: " + this.entry.category + " - " + this.entry.description);
        this.buttonService.updateButton(
            this.index, 
            this.entry.category, 
            this.entry.amount, 
            this.entry.description, 
            this.entry.trigger_alert
        );
        this.params.closeCallback();
        Toast.makeText("Button settings have been saved.").show();

    }

    /**
     * Close modal without saving
     */
    closeNoSave(): void {
        this.params.closeCallback();
    }

    ngOnInit(): void { }

}
