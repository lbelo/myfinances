import { Injectable } from "@angular/core";
var Sqlite = require("nativescript-sqlite");
import { DatabaseService } from "../core/sqlite.service";

export interface IButtonItem {
    category: string;
    icon: string;
    description: string;
    amount: number;
    trigger_alert: boolean;
}

@Injectable()
export class ButtonService {
    private buttons = new Array<IButtonItem>();

    constructor (private database: DatabaseService) {
    }

    getButtons() {
        return this.database.getDBConnection().then(db => {
            db.resultType(Sqlite.RESULTSASOBJECT);
            return db.all('SELECT * FROM buttons');
        });
    }

    getButton(id: number) {
        //return this.buttons.filter((button) => button.id === id)[0];
    }

    addButton(button: IButtonItem) {
    }

    /**
     * Update button setting entry
     * @param number index
     * @param string category 
     * @param number amount 
     * @param string description 
     * @param boolean trigger_alert
     */
    updateButton(index: number, category: string, amount: number, description: string, trigger_alert: boolean) {        
        // push to database
        this.database.getDBConnection().then(db => {
            db.execSQL(
                'UPDATE buttons SET category = ?, amount = ?, description = ?, trigger_alert = ? WHERE id = ?',
                [category, amount, description, trigger_alert? 1 : 0, index]
            );
        });
    }
}