import { Injectable } from "@angular/core";
var Sqlite = require("nativescript-sqlite");

export const DB_FINANCES = 'finances';

@Injectable()
export class DatabaseService {
    private database: any = null;

    constructor() {
        // initialize database
        this.initializeDB();
    }

    public getDBConnection() {
        if (this.database === null) {
            this.database = new Sqlite(DB_FINANCES);
        }

        return this.database;
    }

    public closeDBConnection() {
        this.getDBConnection().then((db) => {
            db.close();
        });
    }

    /** 
     * Create database 
     */
    public initializeDB() {
        this.getDBConnection().then(
            db => {
                db.execSQL(
                    "CREATE TABLE IF NOT EXISTS buttons (id INTEGER PRIMARY KEY AUTOINCREMENT, category TEXT, icon TEXT, description TEXT, amount REAL, trigger_alert INTEGER)"
                )
                .then(
                    () => {}, 
                    error => {
                        console.log("CREATE TABLE ERROR", error);
                    }
                );

                db.execSQL(
                    "CREATE TABLE IF NOT EXISTS expenses (id INTEGER PRIMARY KEY AUTOINCREMENT, category TEXT, description TEXT, amount TEXT, date TEXT)"
                )
                .then(
                    () => {}, 
                    error => {
                        console.log("CREATE TABLE ERROR", error);
                    }
                );
            }
        );

        this.initializeSeedData();
    }

    /**
     * Add initial seed data
     */
    private initializeSeedData() {
        let button_count = 100;

        this.getDBConnection()
        .then(
            db => {
                // check for previous settings
                db.get('SELECT count(*) FROM buttons')
                .then(
                    row => {
                        button_count = row[0];
                        this.getDBConnection()
                        .then(
                            db => {
                                // create button settings if none have been defined
                                if (button_count < 1) {
                                    db.execSQL(
                                        'INSERT INTO buttons (amount, category, description, icon, trigger_alert) ' +  
                                        'VALUES ' + 
                                        '(0, "FOOD", " ", "f0f4", 1),' +
                                        '(0, "FARE", " ", "f1ba", 1),' +
                                        '(0, "FUN", " ", "f118", 1),' +
                                        '(0, "MISC", " ", "f128", 1),' +
                                        '(0, "BILLS", " ", "f0d6", 1),' +
                                        '(0, "EXTRA", " ", "f002", 1),' +
                                        '(0, "Weapons", " ", "f002", 1),' +
                                        '(0, "Refund to Wedding", " ", "f005", 1),' +
                                        '(0, "Wedding", " ", "f004", 1)' 
                                    );
                                }
                            }
                        )
                    }
                );
            }
        );  
    }
}