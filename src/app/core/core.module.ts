import { NgModule } from "@angular/core";
import { DataService } from "./data.service";
import { ButtonService } from "./button.service";
import { DatabaseService } from "./sqlite.service";

@NgModule({
    providers: [
        DataService,
        ButtonService,
        DatabaseService
    ]
})
export class CoreModule { }

// Firebase Paths
export const FIREBASE_DB_BUTTONS  = "/buttons";
export const FIREBASE_DB_EXPENSES = "/expenses";
