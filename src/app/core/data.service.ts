import { Injectable } from "@angular/core";
import * as firebase from "nativescript-plugin-firebase";
var Sqlite = require('nativescript-sqlite');
import { DatabaseService } from "./sqlite.service";

export interface IDataItem {
    id: number;
    name: string;
    amount: number;
    category: string;
    description: string;
    date: string;
}

@Injectable()
export class DataService {

    constructor (private database: DatabaseService) {
    }

    // TODO temporary data, remove when firebase is fully integrated 
    items = new Array<IDataItem>(
        {
            id: 1,
            name: "Item 1",
            amount: 0,
            category: "food",
            description: "Description for Item 1",
            date: "2014-12-12 10:12"
        }
    );


    getItem(id: number): IDataItem {
        return this.items.filter((item) => item.id === id)[0];
    }

    /**
     * Add an expense list entry
     * @param string category 
     * @param number amount 
     * @param string description 
     */
    addItem(category: string, amount: number, description: string) {
        var d      = new Date();
        var year   = d.getFullYear();
        var month  = ('0' + (d.getMonth()+1)).slice(-2);
        var day    = ('0' + d.getDate()).slice(-2);
        var hour   = ('0' + d.getHours()).slice(-2);
        var minute = ('0' + d.getMinutes()).slice(-2);
        var date   = year + '-' + month + '-' + day + ' ' + hour + ':' + minute;

        // push to firebase
        firebase.push("/expenses", {
            amount: amount,
            category: category,
            description: description,
            date: date
        }).then(
            () => {
                this.database.getDBConnection()
                .then(
                    db => {
                        db.execSQL(
                            'INSERT INTO expenses (amount, category, description, date) VALUES (?,?,?,?)',
                            [amount, category, description, date]
                        );
                    }
                );
            }
        );
    }

    editItem(id: number, description: string) {
        //this.items.every()
    }

    getLatestItems() {
        return this.database.getDBConnection()
        .then(
            db => {
                db.resultType(Sqlite.RESULTSASOBJECT);
                return db.all('SELECT * FROM expenses ORDER BY date DESC LIMIT 3');
            }
        );
    }


    /**
     * Update button setting entry
     * @param number index
     * @param string category 
     * @param number amount 
     * @param string description 
     * @param boolean trigger_alert
     */
    updateButton(index: number, category: string, amount: number, description: string, trigger_alert: boolean) {        
        // push to firebase
        firebase.update("/buttons/" + index, {
            amount: amount,
            category: category,
            description: description,
            trigger_alert: trigger_alert
        });
    }

    /**
     * Get total sum for today
     */
    getTodaysTotal() {
        return this.database.getDBConnection()
        .then(
            db => {
                db.resultType(Sqlite.RESULTSASOBJECT);
                return db.get("SELECT sum(amount) as total FROM expenses WHERE date(date) = date('now')");
            }
        );
    }

    /**
     * Get total category sums for today
     */
    getTodaysCategorySums() {
        return this.database.getDBConnection()
        .then(
            db => {
                db.resultType(Sqlite.RESULTSASOBJECT);
                return db.all("SELECT category, sum(amount) as total FROM expenses WHERE date(date) = date('now') GROUP BY category");
            }
        );
    }
}
