import { IButtonItem } from "./button.service";

export class Button implements IButtonItem {
    category: string;
    icon: string;
    description: string;
    amount: number;
    trigger_alert: boolean;
}