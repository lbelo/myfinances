import { Component, OnInit } from "@angular/core";
import { DataService } from "../core/data.service";
import { NumberFormat } from "nativescript-intl";

@Component({
    selector: "Chart",
    moduleId: module.id,
    templateUrl: "./chart.component.html"
})
export class ChartComponent implements OnInit {
    pageData: { Category: string, Amount: number }[];
    todaysTotal: number;
    categorySums: any;
    dateToday: string;

    constructor(private dataService: DataService) {
        // Use the component constructor to inject providers.

        var d      = new Date();
        var year   = d.getFullYear();
        var month  = ('0' + (d.getMonth()+1)).slice(-2);
        var day    = ('0' + d.getDate()).slice(-2);
        this.dateToday = year + '-' + month + '-' + day;
    }

    ngOnInit(): void {
        this.refreshTotals();
    }

    refreshTotals() {
        this.dataService.getTodaysTotal().then(
            result => {
                this.todaysTotal = result.total;
            }
        );

        this.dataService.getTodaysCategorySums().then(
            categories => {
                this.categorySums = categories;        

                let chartData = [];
                // prepare chart data
                for (let category of categories) {
                    chartData.push({ Category: category.category, Amount: category.total });
                }
                this.pageData = chartData;
            }
        );
    }

    // convert number into currency format
    toCurrency(rawNumber: number)  {
        return new NumberFormat('en-US', {'style': 'currency', 'currency': 'USD', 'currencyDisplay': 'symbol'}, '#,##0.00').format(new Number(rawNumber));
    }
}
