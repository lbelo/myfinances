import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { BrowseComponent } from "./browse/browse.component";
import { HomeComponent } from "./home/home.component";
import { ItemDetailComponent } from "./item-detail/item-detail.component";
import { ChartComponent } from "./chart/chart.component";
import { SettingsComponent } from "./settings/settings.component";
import { EntryComponent } from "./entry/entry.component";
import { SettingEditComponent } from "./setting-edit/setting-edit.component";

export const COMPONENTS = [BrowseComponent, HomeComponent, ItemDetailComponent, ChartComponent, SettingsComponent, EntryComponent, SettingEditComponent];

const routes: Routes = [
    { path: "", redirectTo: "/(homeTab:home//browseTab:browse//chartTab:chart//settingsTab:settings)", pathMatch: "full" },

    { path: "home", component: HomeComponent, outlet: "homeTab" },
    { path: "browse", component: BrowseComponent, outlet: "browseTab" },
    { path: "chart", component: ChartComponent, outlet: "chartTab" },
    { path: "settings", component: SettingsComponent, outlet: "settingsTab" },
    { path: "entry", component: EntryComponent },
    { path: "setting-edit", component: SettingEditComponent },

    { path: "item/:id", component: ItemDetailComponent, outlet: "browseTab" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
