/**
 * Component Popup modal when a home screen button is pressed.
 */
import { Component, OnInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { DataService } from "../core/data.service";
import { IButtonItem } from "../core/button.service";
import * as Toast from "nativescript-toast";

@Component({
    selector: "Entry",
    moduleId: module.id,
    templateUrl: "./entry.component.html",
    styleUrls: ["./entry.component.scss"]
})

export class EntryComponent implements OnInit {
    entry: IButtonItem;

    constructor(private params: ModalDialogParams, private dataService: DataService) {
        // value must be initialized after being declared otherwise you can't use it
        this.entry = {
            category: params.context.button.category,
            icon: params.context.button.icon,
            description: params.context.button.description,
            amount: params.context.button.amount,
            trigger_alert: params.context.button.trigger_alert
        };
    }

    ngOnInit(): void { }

    closeAndSave(): void {
        this.dataService.addItem(this.entry.category, this.entry.amount, this.entry.description);
        
        // close the modal
        this.params.closeCallback();

        // show notification that save is completed
        Toast.makeText("Expense has been logged.").show();
    }

    /**
     * Close modal without saving
     */
    closeNoSave(): void {
        this.params.closeCallback();
    }
}
