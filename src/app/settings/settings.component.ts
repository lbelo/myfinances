import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { ButtonService, IButtonItem } from "../core/button.service";
import * as firebase from "nativescript-plugin-firebase";
import * as core from "../core/core.module";
import { Button } from "../core/button.model";
import { SettingEditComponent } from "../setting-edit/setting-edit.component";
import { ModalDialogService } from "nativescript-angular/directives/dialogs";
var Sqlite = require("nativescript-sqlite");
import { DatabaseService } from "../core/sqlite.service";
import * as Toast from "nativescript-toast";

@Component({
    selector: "Settings",
    moduleId: module.id,
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {
    buttons: Array<IButtonItem>;

    constructor(
        private buttonService: ButtonService,
        private _modal: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private database: DatabaseService
    ) {
        // Use the constructor to inject services.
    }

    ngOnInit(): void {
        // process the database promise from the service
        this.buttonService.getButtons().then(
            (rows) => { 
                this.buttons = rows;
            }
        );
    }

    /**
     * Show modal for editing button settings
     * @param IButtonItem button 
     * @param number index 
     */
    showEditModal(button: IButtonItem, index: number) {
        //since buttons are listed as 0 based index
        index = index + 1;
        this._modal.showModal(SettingEditComponent, { context: { button, index }, fullscreen: false, viewContainerRef: this._vcRef }).then(() => {
            this.buttonService.getButtons().then(
                (rows) => {
                    this.buttons = rows;
                }
            );
        });
    }

    toIcon(value: string) {
        return String.fromCharCode(parseInt(value, 16));
    }

    // retrieve updated expenses
    syncExpenseList() {
        Toast.makeText("Expense list sync has been started.").show();
        firebase.getValue(core.FIREBASE_DB_EXPENSES).then(
            (result) => {
                //clear expenses table
                this.database.getDBConnection()
                .then(
                    db => {
                        db.execSQL("DELETE FROM expenses");
                        return db;
                    }
                ).then(
                    db => {
                        // Firebase stores each expense entry with a unique key
                        // we get each expense item by that key
                        for (let key in result['value']) {
                            db.execSQL(
                                'INSERT INTO expenses (amount, category, description, date) VALUES (?,?,?,?)',
                                [
                                    result['value'][key]['amount'],
                                    result['value'][key]['category'],
                                    result['value'][key]['description'],
                                    result['value'][key]['date']
                                ]
                            );
                        }

                        Toast.makeText("Expense list sync has been completed.").show();
                        return db;
                    }
                );
            }
        );
    }
}
