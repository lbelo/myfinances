// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic } from "nativescript-angular/platform";

import { AppModule } from "./app/app.module";

import firebase = require("nativescript-plugin-firebase");

platformNativeScriptDynamic().bootstrapModule(AppModule);

// Initialize Firebase
firebase.init({
    //persist should be set to false as otherwise numbers aren't returned during livesync
    persist: true,
    onAuthStateChanged: (data: any) => {
        console.log(JSON.stringify(data))
        if (data.loggedIn) {
            console.log(data.user.uid);
        }
        else {
            console.log("failed login");
        }
    }
}).then(
    function (instance) {
        console.log("firebase.init done");
    },
    function (error) {
        console.log("firebase.init error: " + error);
    }
);